#!/bin/sh

# Pingmon - monitor hosts and domains via ping, DNS, and TCP port tools
# prerequisite: pingmon.cfg (see example)
# usage: TZ=US/Central nohup ./pingmon/pingmon.sh >> pingmon/pingmon.log 2>&1 &

set -o errexit -o nounset -o pipefail #-o xtrace # DEBUG

# Check whether -e is superfluous for handling escape characters
if [ $(echo "\n"|wc -l) -eq 2 ]; then
  ECHO=echo
else
  ECHO='echo -e'
fi

LOGARGS='+%b %e %T'
ISSUESURL='https://gitlab.com/bconway/pingmon/issues'
CONFIG=$(dirname ${0})/pingmon.cfg
hourdelim=0

set_ping_args() {
  case $(uname) in
    OpenBSD) PING='ping -w 1'
             PING6='ping6 -w 1'
             TCPCON='nc -v -w 2';;
    Darwin) PING='ping -W 1000'
            PING6=ping6
            TCPCON='nc -v -G 2 -w 1';;
    Linux) PING='ping -W 1'
           PING6='ping6 -W 1'
           TCPCON='nc -v -w 2';;
    *) ${ECHO} 'Unsupported operating system, exiting.'
       exit 1;;
  esac
}

read_config_file() {
  # Can't proceed if config file doesn't exist
  if [ -f ${CONFIG} ]; then
    . ${CONFIG}
  else
    ${ECHO} 'pingmon.cfg not found, exiting.'
    exit 1
  fi
}

# Assigns: hourdelim
check_delim_set_logdate() {
  # Check time between tests/sleep to place delimiter
  datemin=$(date +%M)
  logdate=$(date "${LOGARGS}")

  # Check for 15 minutes before/after the hour to avoid overlap in delimiting
  if [ ${hourdelim} -eq 0 ] && [ ${datemin} -ge 55 ]; then
    hourdelim=1
  fi

  if [ ${hourdelim} -eq 1 ] && [ ${datemin} -ge 0 ] && \
  [ ${datemin} -le 5 ]; then
    ${ECHO} "${logdate} -----"
    hourdelim=0
  fi
}

# Test ping and ping6 connectivity
# Args: $1 - ping type, $2 - pinghost
ping_test() {
  # Handle non-zero exit
  if [ ${1} == 'ping' ]; then
    pingout=$(${PING} -n -c ${PINGCOUNT} -q ${2} 2>&1) || true
  else
    pingout=$(${PING6} -n -c ${PINGCOUNT} -q ${2} 2>&1) || true
  fi
  # recvcount null if unknown host
  recvcount=$(echo "${pingout}"|fgrep received|awk '{ print $4 }') || true
  # recvstats null if unknown host or 0 replies
  recvstats=$(echo "${pingout}"|fgrep min/avg/max|awk '{ print $4 }') || true

  # Cases: (1) unknown host (yellow), (2) network unreachable (red), (3) replied
  # 0 packets (red), (4) replied above packet and RTT alarm (pass), (5) replied
  # below packet or RTT alarm (yellow)
  if echo ${pingout}|egrep -q "[Uu]nknown host|not known|no address"; then
    ${ECHO} "${logdate} ${1}mon: \033[1;31m${2} unknown host\033[0m"
  elif echo ${pingout}|fgrep -q "Network is unreachable"; then
    ${ECHO} "${logdate} ${1}mon: \033[1;31m${2} network unreachable\033[0m"
  elif [ ${recvcount} -eq 0 ]; then
    ${ECHO} "${logdate} ${1}mon: \033[1;31m${2} received ${recvcount}" \
            'packets\033[0m'
  elif [ ${recvcount} -gt ${ALARMCOUNT} -a $(echo ${recvstats}|awk -F / \
         '{ printf "%d", $2 }') -le ${ALARMRTT} ]; then
    : # pass
  elif [ ${recvcount} -le ${ALARMCOUNT} -o $(echo ${recvstats}|awk -F / \
         '{ printf "%d", $2 }') -gt ${ALARMRTT} ]; then
    recvrange=$(echo ${recvstats}|awk -F / '{ printf "%d-%d", $1, $3 }')
    ${ECHO} "${logdate} ${1}mon: \033[1;33m${2} received ${recvcount} packets" \
            "${recvrange} ms\033[0m"
  else
    log_fallthrough "${1}mon ${2}" "${pingout}"
  fi

  sleep 1
}

# Test DNS resolution
# Args: $1 - dnshost
dns_test() {
  # Use TCP host to differentiate timeout and refused, handle non-zero exit
  hostout=$(host -T -W 2 $(echo ${1}|awk -F @ '{ print $1, $2 }') 2>&1) || true

  # Cases: (1) found address and/or mail, (2) timeout (red), (3) connection
  # refused (yellow), (4) NXDOMAIN (red), (5) SERVFAIL (red), (6) comm error,
  # (7) alias without address, (8) empty response, (9) network unreachable
  if echo ${hostout}|egrep -q "has.*address|mail is handled by"; then
    : # pass
  elif echo ${hostout}|egrep -q \
  "connection timed out|failed: host unreachable"; then
    ${ECHO} "${logdate} dnsmon: \033[1;31m${1} connection timed out\033[0m"
  elif echo ${hostout}|fgrep -q "failed: connection refused"; then
    ${ECHO} "${logdate} dnsmon: \033[1;33m${1} connection refused\033[0m"
  elif echo ${hostout}|fgrep -q "not found: 3(NXDOMAIN)"; then
    ${ECHO} "${logdate} dnsmon: \033[1;31m${1} not found NXDOMAIN\033[0m"
  elif echo ${hostout}|fgrep -q "not found: 2(SERVFAIL)"; then
    ${ECHO} "${logdate} dnsmon: \033[1;31m${1} not found SERVFAIL\033[0m"
  elif echo ${hostout}|fgrep -q "communications error"; then
    ${ECHO} "${logdate} dnsmon: \033[1;33m${1} communications error\033[0m"
  elif echo ${hostout}|fgrep -q "is an alias for"; then
    ${ECHO} "${logdate} dnsmon: \033[1;31m${1} alias without address\033[0m"
  elif echo ${hostout}|fgrep -q "Aliases:"; then
    ${ECHO} "${logdate} dnsmon: \033[1;31m${1} empty response\033[0m"
  elif echo ${hostout}|fgrep -q "network unreachable"; then
    ${ECHO} "${logdate} dnsmon: \033[1;31m${1} network unreachable\033[0m"
  else
    log_fallthrough "dnsmon ${1}" "${hostout}"
  fi

  sleep 1
}

# Test TCP port connectivity
# Args: $1 - tcphost
tcp_test() {
  # Handle non-zero exit
  tcpout=$(${TCPCON} $(echo ${1}|awk -F : '{ print $1, $2 }') 2>&1) || true

  # Cases: (1) successful connection, (2) timeout (red), (3) connection refused
  # (red), (4) NXDOMAIN (red)
  if echo ${tcpout}|fgrep -q "succeeded"; then
    : # pass
  elif echo ${tcpout}|fgrep -q "timed out"; then
    ${ECHO} "${logdate} tcpmon: \033[1;31m${1} connection timed out\033[0m"
  elif echo ${tcpout}|fgrep -q "failed: Connection refused"; then
    ${ECHO} "${logdate} tcpmon: \033[1;31m${1} connection refused\033[0m"
  elif echo ${tcpout}|egrep -q \
  "no address associated with name|or not known"; then
    ${ECHO} "${logdate} tcpmon: \033[1;31m${1} not found NXDOMAIN\033[0m"
  else
    log_fallthrough "tcpmon ${1}" "${tcpout}"
  fi

  sleep 1
}

# Args: $1 - command args, $2 - ping or host output
log_fallthrough() {
  ${ECHO} 'Unrecognized output! Please report the following to' \
          "${ISSUESURL}\n${1}\n${2}"
}

set_ping_args
while true; do
  read_config_file

  check_delim_set_logdate
  for pinghost in ${PINGHOSTS}; do
    ping_test ping ${pinghost}
  done

  check_delim_set_logdate
  for ping6host in ${PING6HOSTS}; do
    ping_test ping6 ${ping6host}
  done

  check_delim_set_logdate
  for dnshost in ${DNSTESTS}; do
    dns_test ${dnshost}
  done

  check_delim_set_logdate
  for tcphost in ${TCPTESTS}; do
    tcp_test ${tcphost}
  done

  check_delim_set_logdate
  sleep ${POLLINT}
done
