# Pingmon

Pingmon is a simple ping, DNS, and TCP port monitoring tool written in
Bourne-compatible shell. It features color-coded output. It is ideal for home
use and can be made to log to a file and watched with tail.

## Monitors:

- pingmon (host monitoring with ICMP echo request, IPv4)
- ping6mon (host monitoring with ICMP echo request, IPv6)
- dnsmon (hostname resolution monitoring, per-server, IPv4/v6 for hostname or
  server)
- tcpmon (TCP port monitoring, IPv4/v6)

## Support Platforms and Shells:

- OpenBSD (sh, ksh, bash)
- Linux (sh, dash, bash)
- FreeBSD (sh)
- Mac OS X (sh, zsh, bash)

## Usage:

There are two main usage patterns for Pingmon, foreground and background logging
with tail.

Foreground:

```
$ ./pingmon.sh
```

Background logging with tail:

```
$ TZ=US/Central nohup ./pingmon.sh >> pingmon.log 2>&1 &
$ tail -[Ff] pingmon.log
```
